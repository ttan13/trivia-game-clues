from fastapi import APIRouter, Response, status
from pydantic import BaseModel
from .categories import CategoryOut, Message
import psycopg

router = APIRouter()

# // Pull out of the database
class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool


class Clues(BaseModel):
    id: str


@router.get("/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_clue(
    clue_id: int,
    response: Response
):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT cl.id, cl.answer, cl.question,
                    cl.value, cl.invalid_count, cl.category_id,
                    cl.canon, ca.id, ca.title, ca.canon
                FROM clues as cl
                INNER JOIN categories AS ca ON(ca.id = cl.category_id)
                WHERE cl.id = %s
                """,
                [clue_id],
            )
            row = cur.fetchone()
            # print("ROW!!!!!!!", row)
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue does not exist"}

            clue = {}
            for i, column in enumerate(cur.description):
                clue[column.name] = row[i]
            # print("CLUE!!!!!", clue)
           
            category = {}
            for i, column in enumerate(cur.description):
                category[column.name] = row[i]
            
            clue["category"] = category
            return clue



@router.get(
    "/api/random-clue", 
    response_model=ClueOut,
    responses={404: {"model": Message}}
)
def get_random_clue(
    valid: bool=True, 
    response: Response
):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            query = """
                SELECT cl.id, cl.answer, cl.question,
                cl.value, cl.invalid_count, cl.category_id,
                cl.canon, ca.id, ca.title, ca.canon
                FROM clues as cl
                INNER JOIN categories AS ca ON(ca.id = cl.category_id)
                WHERE cl.invalid_count = 0
                ORDER BY RANDOM() LIMIT 1;
            """
            if valid == False:
                query = """
                 SELECT cl.id, cl.answer, cl.question,
                    cl.value, cl.invalid_count, cl.category_id,
                    cl.canon, ca.id, ca.title, ca.canon
                    FROM clues as cl
                    INNER JOIN categories AS ca ON(ca.id = cl.category_id)
                    ORDER BY RANDOM() LIMIT 1;
                """
            cur.execute(query)

            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Category not found"}

            clue = {}
            for i, column in enumerate(cur.description):
                clue[column.name] = row[i]

            category = {}
            for i, column in enumerate(cur.description):
                category[column.name] = row[i]
            
            clue["category"] = category
            return clue



# def get_all_clues(self):
#     with psycopg.connect() as conn:
#         with conn.cursor() as cur:
#             cur.execute(
#                 """
#                 SELECT
#                 FROM
#                 ORDER BY 
#                 """
#             )

#             results = []
#             for row in cur.fetchall():
#                 clues = {}
#                 for i, column in enumerate(cur.description):
#                     clues[column.name] = row[i]
#                 results.append(clues)

#             return results